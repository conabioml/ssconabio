# -*- coding: utf-8 -*- 
import os
import nltk
import simplejson
import pickle
import unicodedata
import codecs
import nltk
import types
import unicodedata

from nltk.tag.stanford import StanfordPOSTagger
from optparse import OptionParser
from sys import argv
from nltk.corpus import cess_esp as cess 
from os import listdir
from nltk import BigramTagger as bt 
from nltk import TrigramTagger as tt 
from nltk.tag.stanford import StanfordPOSTagger

java_path = "/usr/bin/java"
os.environ['JAVAHOME'] = java_path
spamodel = './stanford/models/spanish.tagger'
posjar = './stanford/stanford-postagger.jar'
default_cname_features = './feat_cname.JSON'
spanish_postagger = StanfordPOSTagger(model_filename =spamodel,
									  path_to_jar = posjar,
									  encoding='utf8')

### cargamos las opciones a las variables

parser = OptionParser()

parser.add_option("-p", "--posterioriTree", 
	dest="posterioriTree",
	type="string",
	help="Abre un arbol ya entrenado", 
	metavar="FILE")

parser.add_option("-t", "--trainDir", 
	dest="trainDir",
	help="Directorio para entrenar un nuevo arbol", 
	metavar="DIR")
parser.add_option("-v", "--verbose", 
	dest="verbose",
	help="default = True  para cambiar escribe False", 
	default=True,
	metavar="BOOLEAN")

parser.add_option("-e", "--evalDir", 
	dest="evalDir",
	help="Directorio con archivos para la evaluacion", 
	metavar="DIR")

parser.add_option("-c", "--classDir", 
	dest="classDir",
	help="Directorio con archivos para clasificar", 
	metavar="DIR")

parser.add_option("-s", "--nameTree", 
	dest="nameTree",
	help="Nombre de arbol despues de entrenar")

parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")

(options, args) = parser.parse_args()

class relationTree():
	list_tuplas=[]
	##list_tuplas 	
	## AQUI CUARDAMOS LAS  TUPLAS PREVIAS EN CASO DE 
	## QUE CARGEMOS ARCHIVO PARA METER NUEVAMETE AL 
	## GENERADOR DEL ARBOL YA 
	## CON LAS NUEVAS CARACTERISTICAS
		## [({f1:v1,f2:v2,...,fn:vn},class)
		##	({f1:v1,f2:v2,...,fn:vn},class),...,
		##	({f1:v1,f2:v2,...,fn:vn},class)]
	DecisionTreeClassifier=""
	##model={}		 	## Guarda  el dicionario de caracteristicas
	ann=[] 				## Guarda la lista de los archivos ann
	txt=[]				## Gurada la lista de los archivos txt
	list_features={}	
	## Guarda una lista de diccionarios de caracteristicas 
	## Servira para generar resultados
						## La estructura de datos generada se puede ver como:
						##
				
		## { 'f1':[('v1f1','claseV1'),('v2f1',clasev2),...,('vnf1',clasevn)],
		##    'f2':[('v1f2','claseV1'),('v2f2',clasev2),...,('vnf2',clasevn)],
		##     .
		##     .
		##     . 
		##    'fm':[('v1fm','claseV1'),('v2fm',clasev2),...,('vnfm',clasevn)],
		## }
				## se puede leer como una lista de 'm' caracteristicas[features] de 'n' valores
	results={}


	"""docstring for relationTree"""
	def __init__(self):
		# almacenar modelo de clasificación de relación nombres cientificos-nombres comunes
		# ver : https://wiki.python.org/moin/UsingPickle	
		# super(relationTree, self).__init__()
		# self.arg = arg
		print options
		if(options.verbose=="False"):
			options.verbose=False
		if(options.verbose=="True"):
			options.verbose=True

		if not(options.posterioriTree == None ) :
			print "Cargamos archivo ",options.posterioriTree+".p"
			file=options.posterioriTree+".p"
			self.list_tuplas =  pickle.load( open( file, "rb" ) )
			self.DecisionTreeClassifier=nltk.DecisionTreeClassifier.train(self.list_tuplas,binary=True,verbose=options.verbose)

		if not(options.trainDir == None) :
			print "Entrenamos con Dir " ,options.trainDir
			self.list_tuplas=self.list_tuplas+self.train(options.trainDir)
			self.DecisionTreeClassifier=nltk.DecisionTreeClassifier.train(self.list_tuplas,binary=True,verbose=options.verbose)
			print "Árbol generado"
			print self.DecisionTreeClassifier.pseudocode(depth=5)
			#print self.DecisionTreeClassifier
			if not(options.nameTree == None) :
				print "Guardamos como ", options.nameTree+".p"
				pickle.dump(  self.list_tuplas   , open( options.nameTree+".p" , "wb" ))
		
		if not(options.evalDir == None) :
			print "Evaluamos con Dir ", options.evalDir
			self.test_tree(options.evalDir)
	
		if not(options.classDir == None) :
			print "Clasificamos Dir ", options.classDir
			self.class_dir(options.classDir)
		

			
	def readDir(self,url):
		self.ann=[]
		self.txt=[]
		for x in os.listdir(url): 
			if x[len(x)-3:] =="ann":
				self.ann.append(x)
			if x[len(x)-3:]== "txt":
				self.txt.append(x)


		## meter validacion para archivos que no tengan ann 
		## meter validacion para archivos que no tengan txt 
		## meter validacion para archivos que no tengan ann 
		## meter validacion para archivos que no tengan txt 

	def train(self,url):
		self.readDir(url)
		while self.txt!=[]:
			txtAUX = self.txt.pop()
			annAUX = self.ann.pop(self.ann.index(txtAUX[:len(txtAUX)-3]+"ann"))
			txtAUX = url+"/"+txtAUX
			annAUX = url+"/"+annAUX
			print "---------------------------------------------------------------------------"
			print "Entrenando: "
			print txtAUX 
			print annAUX
			list_tuplas=self.create_model(txtAUX,annAUX)
			self.add_to_model(list_tuplas)
			return list_tuplas
			
	
	## genera una lista de nombres y otra de relaciones

	def create_model(self,txt,ann):

		DOC="" ## Documento leido 
		row="" ## anotacion 
		Relations=[] ## lista de las relaciones entre los arguemntos
		Names=[] 	## lista de nombres en particular identificados por la anotacion T
		vector=[] 	
		## si es un nombre comun o cientifico
		##  [ ID ,type ,offsetStart ,offsetEnd , name ]
		## si es una relacion	
		##  [ ID ,class ,Arg1[5] ,Arg2[5] , textline ]
		
		with codecs.open(txt,mode="r",encoding="utf-8") as txtFile:
			DOC = txtFile.readlines()
		with codecs.open(ann,mode="r",encoding="utf-8") as annFile:
			row  = annFile.readline()
			while row != '':
				vector=self.annToVector(row.split(),Names,DOC)
				if vector[0][0]=='T':
					Names.append(vector)
				else: 
					Relations.append(vector)
				vector=[]
				row  = annFile.readline()
			dicionario_general=self.gen_dic(Relations)
			##dicionario_general= [	({'f1':'valf1','f2':'valf2',...,'fn':'valfn',},class),
			##						({'f1':'valf1','f2':'valf2',...,'fn':'valfn',},class),
			##						.
			##						.
			##						.
			##						({'f1':'valf1','f2':'valf2',...,'fn':'valfn',},class)
			##					]
			
		return dicionario_general
	def class_model(self,txt,ann):
		print "class_model"

		DOC="" ## Documento leido 
		row="" ## anotacion 
		Relations=[] ## lista de las relaciones entre los arguemntos
		Names=[] 	## lista de nombres en particular identificados por la anotacion T
		vector=[] 	
		## si es un nombre comun o cientifico
		##  [ ID ,type ,offsetStart ,offsetEnd , name ]
		## si es una relacion	
		##  [ ID ,class ,Arg1[5] ,Arg2[5] , textline ]
		
		with codecs.open(txt,mode="r",encoding="utf-8") as txtFile:
			DOC = txtFile.readlines()
		with codecs.open(ann,mode="r",encoding="utf-8") as annFile:
			row  = annFile.readline()
			print row
			while row != '':
				vector=self.annToVector(row.split(),Names,DOC)
				if vector[0][0]=='T':
					Names.append(vector)
				else: 
					print ""
					print " CLASE =>"+self.DecisionTreeClassifier.classify(self.rel_features(vector))
					print "^^^^^^^^^^^^^^^^^^^^^"
			
				row  = annFile.readline()
			
		return True


	##recibe una lista de relaciones
	##regresa una lista de tuplas [
	##								({f1:v1,f2:v2,...,fn:vn},class),
	##								({f1:v1,f2:v2,...,fn:vn},class),...,
	##								({f1:v1,fn:v2,...,fn:vn},class)
	##  						]
	##
	##
	##recibe una lista de relaciones
	##regresa una lista de tuplas [({features},class)]
	def gen_dic(self,relations):
		lista_dics=[]
		aux={}
		for x in relations:
			aux = self.rel_features(x)
			lista_dics.append((aux,x[1]))
		return lista_dics


	## agrega al modelo existente la informacion de la lista de tuplas	
	def add_to_model(self,list_tuplas):
		print "aqui",list_tuplas

		##verificamos si exitsen la caracteristicas
		if not list_tuplas==[]:
			keys= list_tuplas[0][0].keys()
		
			for key in keys:
				for x in list_tuplas:
					if not(self.list_features.has_key(key)):
						self.list_features[key]=[(x[0][key],x[1])]
					else:
						self.list_features[key].append( (x[0][key],x[1]) )
		###
		###{ 'f1':[('valf1':'class'),('valf1':'class'),...,('valf1':'class')],
		##   'f2':[('valf2':'class'),('valf2':'class'),...,('valf2':'class')],
		##  .
		##  .
		##  .
		##  'fn':[('valfn':'class'),('valfn':'class'),...,('valfn':'class')],
		###}
		###

	def annToVector(self,row,Names,DOC):
		if len(row) > 4 :
			AUX= " ".join(row[4:len(row)])
			while len(row)>4:
				row.pop(len(row)-1)
			row.append(AUX)
		if row[0][0]=='T':
			## si es un nombre comun o cientifico guardamos la informacion
			## ID CLASE offsetInicio offsetfinal , nombre
			return  [ row[0] , row[1], row[2] , row[3] , row[4]  ]
		else:
			A1=self.buscaID(self.arg(row[2]),Names)
			A2=self.buscaID(self.arg(row[3]),Names)
			#[ID, clase ,arg1 ,arg2,LINEA]
			return [row[0],row[1],A1,A2, self.line(A1[2],DOC)  ]
	 
	def buscaID(self,ID,Names):
		for x in Names:
			if ID in x:
				return x
		return "No reference "+ID
	def line(self,star,DOC): #return idx of line in DOC
		star = int ("".join([x for x in star if x.isdigit()]))
		cntF=0
		idx=0
		cntI=0
		while cntF<star:
			cntF+=len(DOC[idx])
			cntI=cntF-len(DOC[idx])
			idx+=1
		return (cntI,DOC[idx-1])

	def arg(self,p):
		p=p[5:]
		return p

	def casecode(self,word):
		lc_diacritics = [ord(u) for u in u'áéíóúüñ']
		uc_diacritics = [ord(u) for u in u'ÁÉÍÓÚÜÑ']
		sb_diacritics = [ord(u) for u in u' ºª,.;_()/&%$!¡¿?"#~<>[]}{+-*0123456789:']
		code = ''
		for char in word:
			o = ord(char)
			if (o in range(65, 91) or o in uc_diacritics):
				if code == '':
					code += 'X'
				if code[len(code)-1]!='X':
					code += 'X'
			if (o in range(97, 123) or o in lc_diacritics):
				if code == '':
					code += 'x'
				if code[len(code)-1]!='x':
					code += 'x'
			if o in sb_diacritics:
				code += char
		return code
	def get_POSline(self,arg1,arg2,line):

		pos=self.getPOS(line[1])

		acu=0
		pos_antes=""
		pos_inter=""
		pos_despues=""
		pos_a1=""
		pos_a2=""
		a1=line[1][int(arg1[2]) - line[0]:int(arg1[3]) - line[0] ]
		a2=line[1][int(arg2[2]) - line[0]:int(arg2[3]) - line[0] ]
		#validacion de contenido [truena cuando el ann no corresponde al txt]
		if arg1[4] != a1 or arg2[4] != a2:
			print "BOOM!"
			print "check:"
			print line	
			print
			print "X____x'"		
			exit(1)
		#Regresa la cantidad de caracteres en la relacion sin  incluir los nombres
		if (arg1[2]<arg2[2]):
			antes=line[1][ 0: int(arg1[2])-line[0]]
			inter= line[1][int(arg1[3]) - line[0]:int(arg2[2]) - line[0] ]
			despues=line[1][int(arg2[3])-line[0]:]			
			pos_antes=self.POS_Words(antes,acu,pos)
			acu+=len(pos_antes)
			pos_a1=self.POS_Words(a1,acu,pos)
			acu+=len(pos_a1)
			pos_inter=self.POS_Words(inter,acu,pos)
			acu+=len(pos_inter)
			pos_a2=self.POS_Words(a2,acu,pos)
			acu+=len(pos_a2)
			pos_despues=self.POS_Words(despues,acu,pos)
		else:
			antes=line[1][ 0: int(arg2[2])-line[0]]
			inter= line[1][int(arg2[3]) - line[0]:int(arg1[2]) - line[0] ]
			despues=line[1][int(arg1[3])-line[0]:]
			pos_antes=self.POS_Words(antes,acu,pos)
			acu+=len(pos_antes)
			pos_a2=self.POS_Words(a2,acu,pos)
			acu+=len(pos_a2)
			pos_inter=self.POS_Words(inter,acu,pos)
			acu+=len(pos_inter)
			pos_a1=self.POS_Words(a1,acu,pos)
			acu+=len(pos_a1)
			pos_despues=self.POS_Words(despues,acu,pos)
		
		tokens=nltk.word_tokenize(line[1])

		return [pos_antes,pos_a1,pos_inter,pos_a2,pos_despues]

	def POS_Words(self,line,acu,p):
			lineTokens=nltk.word_tokenize(line)
			return p[acu:acu+len(lineTokens)]
				
	def patron_incluyente(self,arg1,arg2,line):
		if (arg1[2]<arg2[2]):
			#contenido entre nombres Excluyente
			#inter= line[1][int(arg1[3]) - line[0]:int(arg2[2]) - line[0] ]
			patron= arg1[1]+" "+ self.casecode(line[1][int(arg1[3]) - line[0]:int(arg2[2]) - line[0] ]) +" "+arg2[1]
		else:	
			#contenido entre nombres Excluyente
			patron= arg2[1]+" "+ self.casecode(line[1][int(arg1[3]) - line[0]:int(arg2[2]) - line[0] ]) +" "+arg1[1]	
		return patron	
	def patron_NO_incluyente(self,arg1,arg2,line):
		if (arg1[2]<arg2[2]):
			#contenido entre nombres Excluyente
			#inter= line[1][int(arg1[3]) - line[0]:int(arg2[2]) - line[0] ]
			patron= self.casecode(line[1][int(arg1[3]) - line[0]:int(arg2[2]) - line[0] ])
		else:	
			#contenido entre nombres Excluyente
			patron= self.casecode(line[1][int(arg1[3]) - line[0]:int(arg2[2]) - line[0] ]) 	
		return patron


	def casecode_vocals():
		s=""


		return s
	
	def rel_features(self,r):
		print " "
		print r[2][4],"->",r[3][4] , " es " ,r[1] , "... " 
		print " "	
		print "espera..."
		"""r 	= [ ID , CLASE  , arg1[] , arg2[] , (offsetlinea, linea) ]"""
		"""argX = [ ID , CLASE ,offsetInicio ,offsetfinal , nombre]"""
		"""fR 	= [pos_antes,pos_a1,pos_inter,pos_a2,pos_despues]"""
		
		fR=self.get_POSline(r[2],r[3],r[4])
		print "........................."
		print r[4][1]
		print "........................."
		feats={}
		feats['class_A1']=r[2][1]
		feats['class_A2']=r[3][1]
		feats['casecode_A1'] = self.casecode(r[2][4])
		feats['casecode_A2'] = self.casecode(r[3][4])
		""" features distancias"""
		feats['Nwords_before_Args']= self.intervals(len(fR[0]))
		feats['Nwords_between_Args']= self.intervals(len(fR[2]))
		feats['Nwords_after_Args']= self.intervals(len(fR[4]))
		""" features tam_argumentos :describe cuantas palabras forman el arg."""
		feats['Npals_arg1']=len(fR[1])
		feats['Npals_arg2']=len(fR[3])
		"""features patron   NX casecode NX """
		feats['patronI']=self.patron_incluyente(r[2],r[3],r[4])
		feats['patronNoI']=self.patron_NO_incluyente(r[2],r[3],r[4])
		"""features POS"""
		feats['POS_Arg1'] = fR[1]
		feats['POS_interargs'] = fR[2]
		feats['POS_Arg2'] = fR[3]
		# Bag of words:
		#bow_dict = self.cname_bow(r[4][1])
		#merge diccionario actual con bag of words
		z = feats.copy()
		#z.update(bow_dict)
		return z
	def getPOS(self,texto):
		#####
		##### buscar solucion para error cuando el nombre comun esta entre
		##### comillas \u201c \u201d
		#####
		punct = u',.;_()/&%$!¡¿? " “ ” #~<>[]}{+-*: \u201c \u201d'
		tokens = nltk.word_tokenize(texto)
		
		tagger = spanish_postagger.tag(tokens)
		
		s = ""
		for (word, tag) in tagger:
			if word in punct:
				s = s + word
			else:
				s = s + tag[0]
		return s

	def intervals(self, x):

		if x==0:
			return 'Any.'
		if x < 5 and not 0:
			return '1 to 5'
		if x >= 5 and x <=10:
			return '5 to 10'
		if x > 10:
			return 'more than 10'


	def cname_bow(self, text):
		BOW_features = {}
		tok_text = nltk.word_tokenize(text)
		lower_toks = [w.lower() for w in tok_text]

		bow_read = self.read_bow(bow_file=default_cname_features)
		for word in bow_read:
			BOW_features['contains({})'.format(word.encode('UTF-8'))] = (word in lower_toks)
		return BOW_features

	def read_bow(self, bow_file):
		try:
			with open(bow_file, 'rb') as f:
				j = simplejson.load(f)
			return j
		except Exception, e:
			raise e


	## la evaluación que genera nltk solo regresa accoracy
	## vamos a implenetar un metodo que nosde una matriz de confucion
	##
	def test_tree(self,url):
		list_tuplas_temp=[]
		list_tuplas=[]
		vv=0
		fv=0
		ff=0
		vf=0

		if (self.DecisionTreeClassifier==""):
			print "árbol vacio"
			print "Se debe entrenar antes de poder evaluar"
		else:
			self.readDir(url)
			while self.txt!=[]:
				txtAUX = self.txt.pop()
				annAUX = self.ann.pop(self.ann.index(txtAUX[:len(txtAUX)-3]+"ann"))
				txtAUX = url+"/"+txtAUX
				annAUX = url+"/"+annAUX
				print "---------------------------------------------------------------------------"
				print "Evaluando: "
				print txtAUX 
				print annAUX
				list_tuplas_temp=self.create_model(txtAUX,annAUX)
				list_tuplas=list_tuplas+list_tuplas_temp
			for x in  list_tuplas :

				y=self.DecisionTreeClassifier.classify(x[0])
				###print y + " -> "+ x[1]
				if(y==x[1]):
					if (x[1]=="correcto"):
						vv=vv+1
					else:
						ff=ff+1
				else:
					if x[1]=="correcto":
						fv=fv+1
					else:
						vf=vf+1
			print "Matriz de Confucion generada"
			print "-------v------f---"			
			print "------------------"
			print "v--|",vv,"|",vf,"|"
			print "------------------"
			print "f--|",fv,"|",ff,"|"
			print "------------------"
			print "------------------"
			print "accuracy"
			print nltk.classify.accuracy(self.DecisionTreeClassifier,list_tuplas)
			
			
		return (vv,vf,fv,ff)


	def class_dir(self,url):
		list_tuplas_temp=[]
		list_tuplas=[]
		
		if (self.DecisionTreeClassifier==""):
			print "árbol vacio"
			print "Se debe entrenar antes de poder clasificar"
		else:
			self.readDir(url)
			while self.txt!=[]:
				txtAUX = self.txt.pop()
				annAUX = self.ann.pop(self.ann.index(txtAUX[:len(txtAUX)-3]+"ann"))
				txtAUX = url+"/"+txtAUX
				annAUX = url+"/"+annAUX
				print "---------------------------------------------------------------------------"
				print "Clasificando: "
				print txtAUX 
				print annAUX
				self.class_model(txtAUX,annAUX)
						

				


t=relationTree()
