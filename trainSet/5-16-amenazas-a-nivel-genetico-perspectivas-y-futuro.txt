5.16 amenazas a nivel genético perspectivas y futuro
Cecilia Alfonso Corrado 
Ricardo Clark Tapia 

Introducción 

La diversidad genética (carga genética) es la variación de los genes dentro de cada especie (Groom et al., 2006).
Abarca determinadas poblaciones de la misma especie (como las decenas de variedades tradicionales de maíz en México) o la variación genética de una población (que es baja en especies amenazadas o en peligro de extinción y elevada en especies no amenazadas).
La función de la diversidad genética es mantener un reservorio de condiciones que permitan la adaptación (“mecanismo que permite a las poblaciones adquirir características favorables en términos de sobrevivencia y fecundidad”) a cambios del ambiente y de no lograrlo es probable que se extinga (Eguiarte, 1986).
Sin embargo, la pérdida y modificación del hábitat pueden generar una disminución de la diversidad genética (erosión genética) reduciendo el potencial adaptativo de las especies (Spielman et al., 2004) por lo que es necesario incrementar nuestros esfuerzos en neutralizar este fenómeno.
En México, recientemente se ha dado un impulso de manera considerable al estudio de los recursos genéticos vegetales y animales, prueba de ello es el incremento anual de los trabajos científicos referentes a este tema que aparece reportado en la base de datos de ISI Web Knowledge v.7.8 (Thomson Scientific, 2007).
Innegablemente, el futuro del hombre está ligado a la conservación y preservación de la naturaleza y definitivamente en caso de no tomar acciones inmediatas se vislumbra una extinción significativa de especies y consecuentemente una reducción de la riqueza de genes a nivel regional, nacional y mundial.
La extinción es un proceso biológico común que se produce de manera natural (i.e. por erupciones volcánicas, cambio climático, entre otros), sin embargo, la modificación o pérdida del hábitat debido a actividades humanas (desmonte para agricultura, ganadería, actividades forestales u otros) ha acelerado las tasas de extinción.
Por ejemplo, Ehrlich y Ehrlich (1981) sugieren que en los últimos 400 años, debido a la actividad humana, se han incrementado entre 5% y 50% las tasas de extinción de aves y mamíferos.
Ante un cambio del ambiente producido de manera natural o por causas antropogénicas, cada especie debe adaptarse.
Sin embargo, para que la especie se adapte y continúe con su evolución, debe tener variación genética y la manera de entender estos mecanismos es a través de estudios genéticos, específicamente de genética de poblaciones (Eguiarte y Piñero, 1999).

Genética de poblaciones 

La genética de poblaciones es una herramienta que permite cuantificar los niveles de variación genética de las poblaciones (Hartl y Clark, 2007) auxiliándose en el uso de marcadores moleculares, los cuales permiten identificar y caracterizar un genotipo determinado a partir de proteínas (isoenzimas) o a partir de ADN (RAPDs, RFLP, microsatélites, entre otros).
Diversos estudios han documentado que las especies endémicas amenazadas o cuyas poblaciones han sido severamente fragmentadas, son potencialmente más vulnerables a la extinción que las especies con amplia distribución o escasamente afectadas por actividades antropogénicas (Byers, 1995; Oostermeijer et al., 2003; Clark-Tapia, 2004).
Lo anterior debido a que son propensas a presentar: 1) altos niveles de endogamia (apareamientos entre individuos cercanamente emparentados) y 2) una reducción en el flujo génico (intercambio de genes de una población a otra por migración de individuos), que produce una alta diferenciación genética entre poblaciones (Clark-Tapia, 2004; Clark-Tapia et al., 2005).
En el estado de Aguascalientes, existen pocos estudios de variación genética que evalúen el estatus genético de las especies (Alfonso-Corrado et al., 2004; Alfonso-Corrado et al., en revisión) por lo que es necesario incrementar nuestro conocimiento genético en el Estado, especialmente de las especies de fauna y flora prioritarias de conservación (ver tema 15. Especies en riesgo y prioritarias, Cap.
5) que están siendo afectadas principalmente por la pérdida y modificación del hábitat.
En Aguascalientes, la conversión de una gran extensión de los bosques, matorrales y pastizales a tierras agropecuarias, urbanas o industriales ha transformado la cubierta vegetal a un paisaje heterogéneo, ocasionando una pérdida o destrucción del ambiente natural y formando fragmentos de vegetación rodeados de áreas de cultivo, pastizales, vegetación secundaria o zonas urbanas.
Esto a mediano y largo plazo puede provocar que los ambientes naturales fragmentados vean amenazada la existencia de sus especies, debido a que la fragmentación puede limitar su dispersión, impidiendo que muchas de ellas recolonicen los fragmentos aislados, lo cual a su vez modifica la riqueza y diversidad de especies nativas (Primack, 1995).
La modificación del hábitat puede tener serias implicaciones genéticas a largo plazo en las poblaciones que han quedado aisladas.
Por ejemplo, puede provocar que las plantas generen semillas de menor calidad, debido a que poblaciones pequeñas y aisladas son más propensas a experimentar cruzas endogámicas (entre individuos emparentados).
Esto puede favorecer la homocigosis y provocar efectos letales o subletales en algunos caracteres de las especies (Eguiarte y Piñero, 1999; Henríquez, 2004).
Adicionalmente, poblaciones pequeñas y aisladas genéticamente son propensas a sufrir deriva génica, es decir, sufrir efectos deletéreos que resultan del muestreo de gametos de generación en generación, reduciendo los niveles de variación genética y consecuentemente poniendo en riesgo la supervivencia de la especie.

Conclusiones 

La pérdida de diversidad genética ocasiona que las especies tengan menos posibilidades de adaptarse a nuevos ambientes y, por consiguiente, puede dar como resultado una pérdida de la diversidad genética de una población, la pérdida de las especies y la pérdida de la diversidad biológica dentro de una comunidad.
Desafortunadamente, las implicaciones genéticas de la modificación y la pérdida del hábitat en Aguascalientes son inciertas debido a la escasez de estudios, por lo que solamente se pueden realizar especulaciones y preguntas para estudios genéticos posteriores.
Por ejemplo: ¿las especies prioritarias de Aguascalientes presentan niveles de variación genética altos?, ¿ha afectado el establecimiento de cercas con malla ciclónica la variación genética de la fauna en la Sierra Fría?, ¿las poblaciones fragmentadas y aisladas del Estado tienen menos niveles de variación genética que las que presentan una distribución continua?, ¿tiene efectos en la variación genética el tamaño del fragmento poblacional?
En este sentido, es necesario insistir en la importancia de implementar a futuro estudios de esta índole, con la finalidad de ampliar los conocimientos genéticos tan pobremente estudiados en el Estado, sobre todo, si se desea efectuar a tiempo estrategias adecuadas de conservación y manejo de las especies.