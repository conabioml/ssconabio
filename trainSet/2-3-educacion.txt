2.3 educación 
Tomás Ramírez Reynoso 

En este apartado se presentan varios aspectos relacionados con la educación de la población de Aguascalientes, aprovechando la reciente publicación de los resultados definitivos del II Conteo General de Población y Vivienda, 2005; así como del Instituto de Educación de Aguascalientes (IEA).

Analfabetismo 

La tasa estatal de analfabetismo, que es de 4.2%, constituye la mitad del valor promedio nacional (8.4%) y es la quinta más baja entre las entidades federativas.
No obstante, presenta amplias variaciones a escala municipal, como se puede apreciar en la figura 2.3.1.
La tasa más baja (3.2%) correspondió al municipio de Aguascalientes y al de Calvillo la más alta (8.2%); la diferencia entre ambos significa un atraso de 25 años para este último municipio, pues su nivel de analfabetismo actual lo tenía el municipio de Aguascalientes a principios de la década de los ochenta del siglo pasado.
La tasa de analfabetismo entre las mujeres fue de 4.5% y entre los hombres de 3.8%; sin embargo, se aprecia que en Asientos, Calvillo y Tepezalá, el analfabetismo masculino fue mayor al femenino.
Por grupos de edad se tiene que entre las personas más jóvenes (entre 15 y 39 años) es mayor el analfabetismo en los varones, pero que a partir de los 40 años es superior en las mujeres.
La estructura porcentual de las personas analfabetas conforme a su edad indica que: 13.0% tenían de 15 a 29 años; 18.2%, de 30 a 44 años; 23.3%, de 45 a 59 años y 45.5%, de 60 años en adelante.


Asistencia escolar 

Datos del IEA (2006) indican que al inicio del ciclo escolar 2005-2006 había 340 161 alumnos inscritos en Aguascalientes desde el nivel de preescolar hasta el de posgrado.
Pero para apreciar la magnitud de la asistencia y la no asistencia escolar resulta más útil recurrir a la información del II Conteo de Población del Instituto Nacional de Estadística y Geografía, referida a octubre de 2005.
De acuerdo con esta fuente (Instituto Nacional de Estadística y Geografía, 2006b), entre los menores de diez años de edad había una cobertura escolar casi completa; sin embargo, al considerar el total de los menores en edad de cursar la educación básica (desde cinco hasta 14 años), se tiene que 4.5% de ellos no acudía a la escuela.
Esto es, que prácticamente uno de cada veinte menores en edad de cursar la educación básica estaba fuera del sistema escolar.
Los datos reportan la presencia de una relación directa entre la edad y la no asistencia a la escuela, pues en la medida que aumenta la primera crece la segunda.
Así, entre los adolescentes de 15 a 19 años de edad, la no asistencia escolar se dispara a 49.1% y entre los jóvenes de 20 a 24 años alcanza 78.2%.

El nivel de escolaridad 

Este indicador constituye el resumen de la situación educativa de cualquier lugar y se refiere al número promedio de años de instrucción escolar aprobados por las personas de 15 años de edad en adelante.
De acuerdo con el XII Censo General de Población y Vivienda, en el año 2000 la escolaridad promedio en el estado de Aguascalientes era de ocho años (8.2 para los hombres y 7.8 para las mujeres).
Los resultados del II Conteo de Población (Instituto Nacional de Estadística y Geografía, 2006c) indican que cinco años después, en octubre de 2005, la escolaridad promedio en la entidad se incrementó en 0.7 años, al ascender a 8.7 (8.8 para los hombres y 8.6 para las mujeres), que apenas se acercan a la secundaria terminada.
A escala municipal hay distancias verdaderamente dramáticas, pues entre los municipios de mayor y de menor escolaridad (Aguascalientes y Calvillo) existen tres años de diferencia (9.3 y 6.3, respectivamente), que significan el nivel de secundaria completo.
Es importante tener en cuenta que la Organización para la Cooperación y el Desarrollo Económico (OCDE), de la que México es integrante, recomienda a sus países agremiados alcanzar una escolaridad promedio de al menos 12 años (equivalente a la preparatoria terminada), para que sus pobladores estén en posibilidad de acceder a las oportunidades de desarrollo que la actual etapa de globalización conlleva.
Como podemos apreciar por la información previa, Aguascalientes se encuentra muy lejos de alcanzar ese promedio de escolaridad recomendado.


Conclusión

Aguascalientes enfrenta, en el ámbito educativo, uno de sus retos fundamentales, del que se desprenden tres grandes áreas de oportunidad.
En primer lugar, es necesario incrementar la asistencia escolar de los adolescentes y de los jóvenes, encontrando mecanismos para que éstos permanezcan más años en la escuela.
En segundo lugar, se requiere elevar la calidad de la instrucción escolar.
Por último, es de capital importancia cerrar la brecha existente en materia educativa entre la capital de la entidad y el resto de sus localidades. 
