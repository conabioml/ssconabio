4.2 biomarcadores
Roberto Rico Martínez 

Un biomarcador es un cambio en los niveles celulares o moleculares que puede ser usado para predecir efectos ecológicos adversos (negativos) en un individuo o en una población.
Los biomarcadores pueden servir en forma ideal como centinelas o marcadores de exposición al demostrar la presencia de tóxicos biodisponibles y la extensión de la exposición, también funcionan como indicadores potenciales de la contaminación (marcadores de efecto).
Nos indican efectos potenciales de los tóxicos para la salud de los humanos, animales o plantas.
Como herramientas de predicción nos ayudan a predecir los efectos a largo plazo en la salud de las poblaciones o de los ecosistemas.
La predicción se establece por la respuesta del organismo a la presencia de las diferentes concentraciones de la sustancia tóxica, ya sea en experimentos en el laboratorio o con base en las concentraciones ambientales del tóxico.
Se espera de un buen biomarcador que sea rápido y nos ayude a predecir lo más pronto posible los efectos potenciales de un tóxico.
Los biomarcadores nacieron en los años 1950-1970 con los programas de biomonitoreo de la calidad del agua.
A partir de 1980 se monitorea la calidad del agua prestando atención a la biota con lo que se ha logrado demostrar los efectos adversos biológicos en ciertas poblaciones humanas y de vida salvaje.

Pruebas de toxicidad 

El estado de Aguascalientes ha sido sede del desarrollo de pruebas de toxicidad usando invertebrados microscópicos como el rotífero dulceacuícola Lecane quadridentata.
Las pruebas de “toxicidad aguda” consisten en la exposición sin alimento por periodos de 48 h de los organismos vivos en presencia de concentraciones altas (letales) de la sustancia tóxica o la muestra de agua que se quiere analizar.
Las pruebas crónicas consisten en exposiciones más prolongadas de los organismos vivos (días, semanas o meses), a concentraciones no letales de la sustancia tóxica, y usualmente en presencia de alimento.
Estas pruebas son únicas en su tipo en el mundo y existe una base de datos relativamente completa sobre la sensibilidad de Lecane quadridentata a diversos tóxicos orgánicos y metales.
Dentro del desarrollo de estas pruebas se lograron establecer pruebas que usan como biomarcadores de exposición el efecto negativo sobre enzimas esterasas (Pérez-Legaspi y Rico-Martínez, 2002) y fosfolipasas A2 (Pérez-Legaspi et al., 2003).
Dichas pruebas consisten en exposiciones cortas (media hora) de los organismos a una sustancia tóxica o muestra de agua y después de esta corta exposición se agrega un sustrato fluorescente a los organismos vivos que se usan en la prueba y se mide su actividad enzimática mediante la transformación del sustrato que produce una luz intensa en el rango de 520 nm (epifluorescencia).
En caso de una reducción en la actividad enzimática medida a partir de una técnica estandarizada y con una serie de réplicas y un gradiente de concentración de la muestra, se determina la presencia de un efecto adverso en una muestra determinada.
En algunos casos se pueden obtener los valores de CE50 (Concentración en la que hay una reducción del efecto o parámetro a medir de 50%) para muestras ambientales.
En este caso en particular, el valor de CE50 sería la concentración o dilución de la muestra con la que se obtiene una reducción de 50% en la actividad enzimática.
Estas pruebas fueron complementadas con biomarcadores que median el efecto negativo sobre la ingestión en el cladócero dulceacuícola Daphnia magna y fueron aplicados a muestras de aguas residuales en el estado de Aguascalientes.
Una calibración de esta técnica mostró que neonatos de D. magna reducían su tasa de ingestión 50% al ser expuestos a 0.14 mg/L de cobre por una hora (Rico-Martínez et al., 2000).

En el río San Pedro, al poniente de la ciudad de Aguascalientes, dichas pruebas han permitido establecer los niveles de toxicidad existentes en su caudal, el cual es afectado en gran medida por las descargas domésticas, agrícolas e industriales de la zona (figura 4.2.1).
La toxicidad del río San Pedro alrededor de la ciudad y su zona metropolitana varía entre un rango promedio (n = 3) de 0.14 y 5.25 unidades de toxicidad aguda (Santos, 2006; Santos-Medrano et al., 2007).
Si tomamos en cuenta que cualquier valor arriba de 0.3 unidades de toxicidad aguda es considerado tóxico (e inaceptable para la Agencia de Protección Ambiental de Estados Unidos), la conclusión de Santos (2006) es que de los 22 sitios de colecta investigados, once son en promedio tóxicos, ocho moderadamente tóxicos y sólo tres son ligeramente tóxicos en términos de toxicidad aguda.
Estos estudios han sido complementados por otros investigadores quienes han establecido claramente la presencia de contaminantes como metales (IMTA, 1997), valores altos de la demanda bioquímica de oxígeno (DBO), demanda química de oxígeno (DQO), compuestos orgánicos (IMTA, 1997) y bacterias coliformes.
Estos biomarcadores han sido usados en prácticamente todos los municipios del estado de Aguascalientes (Hernández, 2006).
Un estudio de la toxicidad aguda (efecto letal resultado de exposiciones cortas de los organismos de prueba) en muestras de pozos de agua potable en el municipio de Aguascalientes, demostró que los 136 sitios de colecta (de los cuales más de 95% son pozos de agua potable) están libres de toxicidad aguda (Rico-Martínez et al., 2000).
Sin embargo, esta situación puede cambiar drásticamente si tomamos en cuenta que cada vez se perforan los pozos a profundidades mayores y que en la actualidad se perfora a casi 400 m (Castillo, 2003).
La extracción cada vez más profunda de los pozos conlleva el riesgo de extraer vetas de arsénico y otros metales tóxicos, lo que cambiaría drásticamente la calidad de agua de los pozos.
En contraste, las aguas residuales que se descargan al río San Pedro lo han convertido en un verdadero colector de aguas negras que en la inmensa mayoría de su recorrido presenta toxicidad aguda (Santos, 2006).
Los biomarcadores empleados en el estado de Aguascalientes, como es el caso de la inhibición enzimática, suelen ser muy sensibles.
Tal es el caso de la sensibilidad del rotífero Lecane quadridentata al benceno y al plomo cuando se usa como parámetro la inhibición de las enzimas fosfolipasas A2.
Estos biomarcadores pueden ser hasta varios miles o millones de veces más sensibles que las pruebas de toxicidad aguda (Pérez-Legaspi y RicoMartínez, 2003).


Conclusiones 

Los biomarcadores hoy en día son ampliamente utilizados en todo el mundo para usos muy diversos.
Su empleo clásico es el biomonitoreo ambiental.
Sus ventajas incluyen la rapidez y la alta sensibilidad en muchos casos.
Entre las desventajas están su costo que en algunos casos puede ser muy alto y requerir de equipo sofisticado de laboratorio.
Si bien es cierto, los biomarcadores en la mayoría de los casos sólo nos informan de la salud ambiental de un componente del ecosistema, también es cierto que si se saben elegir, los biomarcadores complementan en forma muy efectiva la información que se obtiene sobre la salud de un ecosistema.
En Aguascalientes el uso de biomarcadores para el monitoreo del recurso hídrico ha sido sobresaliente cuando se le compara con el resto del país.
Sin embargo, este uso se debe más a la iniciativa de algunos niveles de gobierno en conjunción con investigadores, que a un programa permanente de monitoreo de la salud ambiental de los cuerpos de agua.
Claramente hay una necesidad de establecer programas permanentes de monitoreo ambiental no sólo en los cuerpos de agua, sino también en ecosistemas terrestres y calidad del aire.
En este sentido se necesita en Aguascalientes y en México desarrollar biomarcadores automatizados que permitan analizar una gran cantidad de muestras en tiempos cortos, sin sacrificar la reproducibilidad y confianza que han ganado algunos biomarcadores. 
