1.9.1 regiones ecológicas 
Instituto Nacional de Estadística y Geografía 

Introducción 

La Comisión para la Cooperación Ambiental está constituida por Canadá, Estados Unidos y México, se creó en los términos del Acuerdo para la Cooperación Ambiental de América del Norte con el objeto de tratar las preocupaciones ambientales regionales, ayudar a prevenir los conflictos comerciales y ambientales potenciales y promover una aplicación efectiva de la legislación ambiental en los tres países (Comisión para la Cooperación Ambiental, 1997).
Como resultado de los trabajos realizados en el seno de dicha Comisión, se definieron los diferentes espacios geográficos que conforman la porción norte del continente americano, con base en la integración de factores geológicos, formas terrestres, suelos, vegetación, clima, fauna silvestre, agua, factores humanos e importancia ambiental.
A esta caracterización se le llama “regionalización ecológica” y constituye un concepto útil para definir políticas y enfocar acciones para el mejoramiento ambiental (Comisión para la Cooperación Ambiental, 1997).
En un primer nivel de clasificación, el subcontinente se subdividió en 15 grandes regiones que se subdividen en otras 52 regiones, las cuales proporcionan mayor detalle en la descripción de las áreas del primer nivel.
A su vez, éstas se subdividen en aproximadamente 200 subregiones, incluidas en un tercer nivel (Comisión para la Cooperación Ambiental, 1997).
En este aparatado se mencionan las subregiones en que se subdivide el estado de Aguascalientes.

Subregiones de Aguascalientes 

El estado de Aguascalientes se localiza dentro de la Región 12 Elevaciones Semiáridas Meridionales, que se extiende desde Arizona y Nuevo México, hasta los Estados del norte y centro de México.
Su paisaje se caracteriza por colinas, valles bajos y planicies.
En general, la vegetación de esta región está dominada por pastizales y en las zonas de transición por matorrales y bosques.
Se subdivide en las siguientes tres subregiones (figura 1.9.1.1 y cuadro 1.9.1.1).
Cañones con selva baja caducifolia de la Sierra Madre Occidental Esta subregión ocupa 3.7% de la superficie del Estado (ver cuadro 1.9.1.1).
Se localiza al suroeste del Estado, en el corredor que forma el cañón de Juchipila y su ramificación hacia el Valle de Huejúcar, en el municipio de Calvillo.
Está formada a partir de sedimentos aluviales y conglomerados que forman parte de la Sierra Madre Occidental.
Se caracteriza por la presencia de elementos propios del matorral subtropical y en ella se localiza una amplia zona agrícola, enfocada principalmente a la producción de guayaba, que está irrigada por las aguas de las presas Malpaso, Media Luna, Ordeña Vieja y La Codorniz.

Piedemontes y planicies con pastizal, matorral xerófilo y bosques de encino y coníferas Es la subregión que abarca la mayor parte del territorio estatal, con 67.2%.
En ella se ubican amplias zonas de pastizales inducidos y de matorral xerófilo en las partes altas del municipio de El Llano, también se encuentra una amplia zona de agricultura de riego que abarca desde las inmediaciones de la ciudad de Aguascalientes, hasta los límites con el estado de Zacatecas.


Sierra con bosques de coníferas y encinos mixtos 

Se localiza en la parte occidental del Estado, abarca 29% de la superficie de Aguascalientes; en ésta se localizan la Sierra Fría y la Sierra del Laurel, que son los macizos montañosos de la entidad, los cuales llegan a alcanzar hasta 3 000 msnm (Instituto Nacional de Estadística y Geografía, 2007) y albergan bosques de encino, encino pino, pino encino y pino.
En específico, el carácter de Área Natural Protegida de la Sierra Fría ha favorecido la recuperación de la vegetación, así como de la fauna silvestre, como el guajolote silvestre y el venado cola blanca.

Conclusiones 

La división en regiones ecológicas ha permitido caracterizar a Norteamérica a partir de una perspectiva ecológica, tomando en cuenta sus características climáticas y permite incluir los tres países que integran este subcontinente a partir de un mandato internacional (Comisión para la Cooperación Ambiental, 1997).
Puesto que Aguascalientes es parte de México, no queda fuera de este contexto, ya que la problemática ambiental no reconoce fronteras, por lo que el estudio y trabajo a partir de las subregiones en que se divide el Estado permitirá realizar labores de monitoreo ambiental.
En este contexto, y considerando que la regionalización ecológica es poco conocida en Aguascalientes, ésta se ha propuesto ante los diferentes actores que participan en la atención a la problemática ambiental (Gobierno del Estado y organizaciones no gubernamentales), con el objeto de que pueda ser un elemento que apoye en la definición de las políticas en materia de conservación de la biodiversidad y cuidado del medio ambiente.
