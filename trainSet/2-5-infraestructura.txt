2.5 infraestructura 
instituto nacional de estadística y Geografía 

Introducción 

Considerando que infraestructura es todo aquello realizado por el hombre con la finalidad de apoyar la generación y el mantenimiento de productos o servicios para conservar y mejorar su calidad de vida, en el estado de Aguascalientes se pueden señalar los siguientes tipos de infraestructura: energética, servicios públicos, industrial, telecomunicaciones, salud, educación, vías de comunicación y transporte, los cuales se describen brevemente a continuación.

Energética 

La entidad cuenta con cinco subestaciones de transmisión eléctrica, con una potencia total superior a dos mil Megavolts-amperes, y 20 para distribución, con potencia de 581 875 Megavolts-amperes.

Servicios públicos de limpia 

Además de las 18 plantas de tratamiento de aguas residuales, en la entidad se cuenta con un relleno sanitario ubicado en el municipio de Aguascalientes y tres estaciones estatales de transferencia de residuos sólidos urbanos, situadas en Villa Juárez, Asientos, Pabellón de Arteaga y Calvillo.

Industrial 

Aguascalientes tiene nueve parques industriales que en conjunto ocupan una superficie de 776 ha de uso industrial y de servicios.

Telecomunicaciones 

La red telegráfica consta de 17 oficinas; la cobertura de líneas telefónicas del Estado es aproximada de 250 000 km; más de 170 comunidades rurales tienen acceso telefónico; existen siete estaciones de microondas de las cuales una es terminal y seis son repetidoras; respecto a las estaciones radiodifusoras existen 21, así como cinco estaciones televisoras y siete oficinas postales.

Salud 
En materia de salud, el Estado cuenta con 140 unidades médicas administradas por el sector público y nueve, por el sector privado; ambas están divididas en aquellas que brindan consulta externa (129) y unidades médicas con servicio de hospitalización (20).

Educación 
El estado de Aguascalientes cuenta con 1 250 planteles educativos, de los cuales 51 son de nivel bachillerato y 21 de nivel superior.
En total existen más de 9 500 aulas, 270 bibliotecas y más de 800 laboratorios.

Vías de comunicación 

Mención especial merece la infraestructura sobre vías de comunicación, pues el Estado cuenta con más de 2 500 km de carreteras, 207 km de líneas de ferrocarril y un aeropuerto con servicio internacional.

Carreteras 

La red carretera tiene una longitud de 2 549 km, de los cuales 818 km son carreteras federales pavimentadas; las carreteras estatales están divididas en 820 km pavimentadas y 91 km revestidas, dando un total de 911 km; 641 km de caminos rurales mejorados y 180 km de terracerías revestidas, dando como promedio 215.61 km por cada 100 km2.
La carretera Panamericana cruza la entidad de sur a norte, comunica a los municipios de Aguascalientes, San Francisco de los Romo, Pabellón de Arteaga, Rincón de Romos y Cosío, los cuales integran la región agrícola del Estado.
De esta carretera se derivan importantes vías estatales que comunican las cabeceras municipales de Jesús María, San José de Gracia, Tepezalá y Asientos, así como a un gran número de localidades.
Por otro lado, la carretera Tampico-Barra de Navidad atraviesa el Estado de oriente a poniente, en su trayectoria cruza los municipios de El Llano, Aguascalientes, Jesús María y Calvillo.
Esta vía comunica la importante zona frutícola de Calvillo y las carreteras estatales que se desprenden de ella unen importantes zonas agrícolas y ganaderas de la entidad.

Ferrocarriles 

En Aguascalientes la longitud de la red ferroviaria es de 207 km.
La línea férrea que pasa por el Estado es la que va de México a Ciudad Juárez y lo hace de sur a norte; su uso es sólo para el acarreo de mercancías y materiales para la industria.
Existen también vías particulares que se unen a esta línea y son de las industrias ubicadas en la entidad; a la altura de la localidad de Loretito se desprende un ramal que va con rumbo a la ciudad de San Luis Potosí.

Aeropuertos 

Existe sólo un aeropuerto en el Estado llamado “Lic.
Jesús Terán Peredo”, está ubicado en el municipio de Aguascalientes y ofrece servicio internacional, además hay un aeródromo en el municipio de San Francisco de los Romo.

Transporte 
Juan Jaime Sánchez Nieves 

En el Estado, el uso de vehículos automotores que funcionan con gas L.P., gasolina y diesel constituye el principal medio de transporte para el desarrollo de actividades en los ámbitos privado, público, productivo y de servicios.
Esto en función de las características de infraestructura urbana y carretera tanto de tipo federal como estatal que prevalecen actualmente en la entidad, toda vez que no se cuenta con servicio de traslado de personas o carga por vía férrea u otros medios.



Como parte de los planes de desarrollo de la obra pública, en los últimos años se ha privilegiado de manera importante la construcción de pasos a desnivel, la sincronización de semáforos en las principales vialidades, así como la ampliación y extensión de avenidas.
Todo esto con la intención de comunicar e integrar a la zona urbana los desarrollos urbanos habitacionales e industriales, principalmente en el municipio de Aguascalientes, lo cual ha incentivado el crecimiento de la mancha urbana.
De igual manera, durante la administración gubernamental estatal 1998–2004 en los municipios del interior se desarrolló la pavimentación de terracerías de acceso a diferentes comunidades, rehabilitándolas como carreteras locales, y así favorecer e incentivar el transporte vía terrestre en todo el Estado.
Por todo lo anterior, así como por el acelerado crecimiento poblacional que se observa en la entidad, se ha incrementado de manera significativa la cantidad de vehículos que circulan en el territorio estatal (cuadro 2.5.1, figura 2.5.1); evidencia de ello es que desde la década de los ochenta, el padrón de vehículos registrados incrementó aproximadamente en 550% (Instituto Nacional de Estadística y Geografía, 2007), contándose en la actualidad con un estimado de 304 348 automotores en circulación (PROESPA, 2007).
Todo esto, sin considerar los que se encuentran de paso o bien de manera permanente y que cuentan con placas tanto de otra entidad de la República Mexicana como del extranjero, de los cuales no se cuenta con algún dato en específico.
El principal uso de los vehículos registrados en la entidad es el particular (60%), siguiendo en orden de importancia los vehículos de carga, las motocicletas y, finalmente, los vehículos dedicados al servicio público de transporte de pasajeros, lo cual puede observarse de manera específica (cuadro 2.5.2, figura 2.5.2;
Instituto Nacional de Estadística y Geografía, 2007). 



