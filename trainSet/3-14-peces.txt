3.14 peces 
Jorge Martínez Martínez 
Antonio Rojas Pinedo 

Introducción 

Los trabajos ictiológicos para el estado de Aguascalientes son muy escasos, siendo el primero de ellos el de Jordan y Snyder (1900).
Posteriormente, se tienen reportes indirectos en otros trabajos para nuestro país, como en los citados por Meek (1902 y 1907); De Buen (1947a, b); Beltrán (1934) y Berdegué (1956).
En los trabajos de Álvarez (1970) se encuentran referencias de peces localizados en la cuenca del río Lerma-Santiago, y aparecen algunos peces encontrados en nuestro Estado.
En el trabajo realizado por Contreras (1978) se reportan 18 especies de peces.
En 1981, Rojas realizó un trabajo sobre la distribución de la ictiofauna del estado de Aguascalientes, en el cual reportó 23 especies, comprendidas en 19 géneros y ocho familias (Rojas, 1981).
Finalmente, Martínez y Rojas (1996) evaluaron los datos referentes al estado actual de la ictiofauna aguascalentense, cuyos resultados se presentan en este trabajo.
Distribución En Aguascalientes existe gran cantidad de drenajes discontinuos y con alta variabilidad en sus características hidrográficas (ver tema 5. Hidrología, Cap. 1), lo cual hace interesante el estudio de los peces en nuestra entidad.
El escurrimiento anual estimado en conjunto para los dos ríos principales del Estado, el río San Pedro (130 millones de metros cúbicos [Mm3]) y el río Calvillo (50 Mm3), es de 180 Mm3 con una cobertura de un área aproximada de 5 mil 430 km2 (SEMARNAT-CNA, 2007).
Debido a las restricciones naturales de orden climático y geológico, los recursos hidráulicos del estado de Aguascalientes no muestran un gran desarrollo, por lo que no se encuentran corrientes fluviales de gran caudal, sino cauces, o lechos de río en que drenan las aguas.
Por lo anterior, la ictiofauna de Aguascalientes se encuentra circunscrita a embalses, charcos, arroyos y bordos permanentes y temporales, lo que le confiere un variado gradiente de ambientes dulceacuícolas (Rojas, 1981).
Las principales presas del Estado por sus extensiones, volúmenes e importancia como hábitats de peces son: Presidente Calles, Abelardo L. Rodríguez, El Saucillo, 50 Aniversario, El Jocoqui, San Blas, La Araña, Malpaso, Media Luna, La Codorniz, Ordeña Vieja, Peña Blanca, El Taray, San Bartolo y La Punta, entre otras (SEMARNAT-CNA, 2007; ver tema 5. Ecosistemas acuáticos, Cap. 3).
La ictiofauna estatal no presenta una diversidad muy grande y muestra un grado bajo de endemismos, pero es importante recalcar el hecho de que el Estado ocupa una amplia zona de transición entre provincias fisiográficas y zonas biogeográficas (Neártica y Neotropical), lo que conduce a un gran número de zonas bióticas distintas (De la Vega, 2003; Miller, 1982, 1986; Miller y Smith, 1986; Schlosser, 1991; Hubbs et al., 1991).

Diversidad 

En la actualidad se tienen registradas 19 especies de peces, distribuidas en 16 géneros y ocho familias, las cuales se muestran detalladamente en los cuadros 3.14.1 y 3.14.2.
El cuadro 3.15.3 muestra la representación proporcional del número de taxones (familias, géneros y especies) de la ictiofauna dulceacuícola del estado de Aguascalientes en relación al total para México.
Las dos familias del Orden Cypriniformes del país se encuentran en el Estado, con siete especies pertenecientes a siete géneros.
Del Orden Cyprinodontiformes sólo tenemos dos familias de las ocho encontradas en México, con sólo tres especies de un total de 184 para nuestro país.
Del Orden Siluriformes, sólo hay una familia con dos especies pertenecientes a un género.
En el Orden Perciformes, uno de los más diversos en el país, encontramos dos familias con cinco especies.
Finalmente, el Orden Atheriniformes está representado por una familia y dos especies en Aguascalientes (Martínez y Rojas, 1996).
En el cuadro 3.14.4 se muestran las familias de peces estrictamente dulceacuícolas con números de géneros y especies registradas en nuestro Estado, y su representación numérica para México, de acuerdo a la región biogeográfica a que pertenecen.
Destacan con cinco familias, de un total de ocho encontradas en el Estado, los representantes neárticos.
La región neotropical presenta sólo dos familias de naturaleza transicional, lo anterior coincide con el aspecto ya señalado anteriormente, respecto a la ubicación geográfica de nuestro Estado entre las dos regiones biogeográficas, con una mayor parte neártica, y una menor proporción correspondiente al suroeste estatal perteneciente a la región neotropical (Martínez y Rojas, 1996; Espinosa et al., 1993a, b).
En el cuadro 3.14.5 se presentan la lista taxonómica y la distribución geográfica de las especies de peces dulceacuícolas registradas en el Estado, señalando los ecosistemas acuáticos muestreados correspondientes en que se encuentran cada una de las especies registradas (Martínez y Rojas, 1996).

Importancia 

La historia de la Mesa Central ha tenido impactos considerables en la composición de la ictiofauna regional.
La cuenca del Lerma-Chapala-Santiago, ubicada en el centro del país, presenta, debido a su situación geográfica, características distintivas altimétricas, climáticas y geosísmicas que se reflejan en la presencia de varias especies típicamente endémicas de peces (De la Vega, 2003).
Desde el punto de vista biogeográfico, los peces dulceacuícolas tienen un alto significado por sus capacidades limitadas de dispersión que los destina a un confinamiento en ciertas cuencas hidrográficas (De la Vega, 2003).
El estado de Aguascalientes se ubica dentro de la cuenca del Lerma (ver tema 5. Hidrología, Cap. 1) en la que quedan incluidas algunas de las regiones hidrológicas prioritarias de acuerdo con la clasificación realizada por la Comisión Nacional para el Conocimiento y Uso de la Biodiversidad (Arriaga et al., 1998, 2002).
Las dos porciones hidrológicas del Estado –particularmente, la más grande, el río San Pedro o Aguascalientes– se caracteriza por su acelerado desarrollo urbano, agrícola e industrial y por la introducción de peces exóticos, lo que ha conducido a un grave deterioro ecológico (ver tema 13. Amenazas a los Peces, Cap. 5).
El impacto del deterioro ambiental se manifiesta por la destrucción y modificación del hábitat acuático, que su vez se traduce en restricción del área distributiva de los peces, disminución de sus poblaciones, reducción de las tasas de natalidad e interrupción del flujo génico, razones que han conducido a que algunas de estas especies se hallen amenazadas.
Estas actividades antropogénicas han modificado los ecosistemas acuáticos afectando su entorno, ya sea por la salinización de los acuíferos y la degradación de los suelos, así como la deforestación.
Asimismo, también se tienen efectos a gran distancia, asociados a la reducción del aporte y calidad de agua dulce y los cambios hidrodinámicos en las cuencas bajas por el represamiento de sus ríos; así como los cambios de uso de suelo con fines agrícolas, y la contaminación a los ríos por agroquímicos y descargas industriales, la reducción de fauna y flora acuáticas, la sobreexplotación, la reducción y el mal manejo del agua por represas (SAGARPA, 2001).

Conclusión 

Históricamente se tenían registradas 23 especies de peces en Aguascalientes; la mayoría ha sido fuertemente impactada por las actividades antropogénicas, por lo que muchas especies han desaparecido localmente.
Podemos concluir que la pérdida de hábitat, la introducción de especies exóticas de peces, el ámbito geográfico restringido y la especialización ecológica de las especies son los principales factores de riesgo para las especies nativas de peces del Estado.
Finalmente, la Carta Pesquera Nacional (SAGARPA, 2001) enfatiza la repoblación de embalses con especies nativas, y recomienda la integración e implementación de políticas ambientales y de desarrollo a nivel regional que controlen de manera efectiva las actividades agrícolas, piscícolas, forestales, industriales, domésticas y la sobreexplotación del recurso hídrico, que en forma conjunta están impactando negativamente los ecosistemas acuáticos a fin de prevenir la violación a las regulaciones entre los diferentes Estados.
