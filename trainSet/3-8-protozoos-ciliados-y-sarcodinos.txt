3.8 protozoos ciliados y sarcodinos 
Rogelio Tiscareño Silva 

Introducción 

Los protozoos perteneces al reino protistas son un grupo extremadamente diverso de organismos eucarióticos, en su mayoría unicelulares, aunque también los hay con estructuras multicelulares o multinucleadas (Sleigh, 1989; Lee, 2000).
Estos seres están lejos de ser simples, ya que cada célula es un ente independiente, tan capaz de satisfacer todos sus requisitos de vida como cualquier planta o animal.
Los protistas se dividen en los siguientes grandes grupos: Dinophyta, Parabasalia, Kinetoplastida, Euglenophyta, Chryptophyta, Opalinata, Heterokontia, Chlorophyta, Haptophyta, Chrytidiomycota, Choanoflagelatta, Rhodophyta, Rhizopoda, Actinopoda, Apicomplexa, Microsporidia, Haplosporidia y Ciliophora (Sleigh, 1989); sin embargo, continúan en proceso de clasificación por tratarse de un grupo polifilético (con organismos de diferente origen) (Hidriu,1996).

Ciliados y sarcodinos

Los ciliados son un tipo de protista que presentan cilios (apéndice móvil) o manifestaciones de ellos como las membranas ondulantes; también tienen sistemas orales, como: himenostma, peristoma, hipostoma, spirostoma, apostoma.
Éstos son los únicos protozoos con micronúcleo y macronúcleo, su reproducción asexual es por fisión binaria transversal y la sexual es por conjugación.
Por su parte, los sarcodinos se distinguen porque emiten pseudópodos (proyecciones de su cuerpo) como estructuras de locomoción, los cuales pueden ser filópodos, rizópodos y axópodos.
Tanto ciliados como sarcodinos tienen una gran cantidad de hábitats, la mayoría son acuáticos de agua dulce, salobre o salada; los hay con cubiertas protectoras o sin ellas; se pueden alimentar de bacterias, de otros protozoos, algas, hongos e incluso de animales invertebrados.

El estudio de los protozoos en el estado de Aguascalientes

El conocimiento de la diversidad de este grupo en el estado de Aguascalientes es incipiente debido a la complejidad en su taxonomía y la falta de especialistas en el ramo.
Actualmente se cuenta con pocas investigaciones donde las especies reportadas corresponden a algunos cuerpos de agua temporales y otros permanentes.
Entre estos trabajos podemos destacar los de Flores (1982 y 1985) y Martínez (1981), quienes reportaron la presencia de Ameboideos, Arcelidos, Diflugidos y Euglifidos en las presas Calles, El Saucillo, Media Luna, Abelardo L. Rodríguez y El Niágara.
Por su parte, Mejía (1983) hizo referencia a la presencia de tres protozoos Codonella cratera, Vorticella sp. y Plagiophyla sp. en la presa Abelardo L. Rodríguez.
Posteriormente, Silva (1986) reportó tres protozoarios en muestras de agua de la presa La Codorniz, Ceratium sp., Trachelomonas sp. y Euglena sp.
Finalmente, Tiscareño (2003, 2005) realizó un estudio en los principales cuerpos de agua del Estado (cuadro 3.8.1), de tal manera que hasta el momento en Aguascalientes se tienen reportadas 29 especies de ciliados y 25 de sarcodinos (cuadro 3.8.2 y 3.8.3).
Cabe mencionar que en todos los cuerpos de agua muestreados se presentaron tanto ejemplares de ciliados como de sarcodinos y la temporada del año que mayor número de especies presentó fue el verano.
Los ciliados que con mayor frecuencia se encontraron fueron Paramecium caudatum, Paramecium sp., Ctedoctema acanthocrypta, Coleps cotospinus, Cinetochilum margaritaceum (cuadro 3.8.2), mientras que los sarcodinos más comunes fueron Saccamoeba limna, Difflugia sp., Amoeba proteus, Arcella vulgaris y Euglypha alveolata (cuadro 3.8.3).
La abundancia, distribución y diversidad de protistas sarcodinos y ciliados que se presentan en cuerpos de agua pequeños puede ser mayor que la de cuerpos de agua de grandes dimensiones, sin embargo, los primeros son más inestables debido a las fluctuaciones de las condiciones ambientales.

Importancia

Los protozoos son importantes desde diferentes puntos de vista.
Desde el punto de vista ecológico, forman parte de las cadenas tróficas acuáticas; otros son simbiontes de rumiantes y termitas ayudándoles a digerir la celulosa (Sleigh, 1989).
Desde el punto de vista médico, existen varias especies que son parásitos de una gran variedad de animales incluyendo al hombre, como por ejemplo: Nosema apis de abejas, Trichdina sp. en peces, Toxoplasma sp. en animales domésticos, Entamoeba sp. o amiba en el hombre; los hay comensales como Entamoeba coli en el hombre, así como también otros que participan en la degradación de contaminantes como Euplotes, Vorticella y Paramecium (Sleigh, 1989).

Conclusiones 

Como se puede observar, la cantidad de organismos reportados aquí –así como el número de cuerpos de agua muestreados– son aún pequeños.
Así pues, para contar con un inventario más completo de este grupo es necesario que se efectúen más investigaciones en otros municipios del Estado, en las que también se deberán abordar aspectos sobre conservación, endemismos, importancia ecológica, entre otros.
