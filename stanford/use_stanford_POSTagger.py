#-*- coding: utf8 -*-

# http://nlp.stanford.edu/software/tagger.shtml 
# http://stackoverflow.com/questions/27047450/meaning-of-stanford-spanish-pos-tagger-tags
# nlp.lsi.upc.edu/freeling/doc/tagsets/tagset-es.html

import nltk

from nltk.tag.stanford import POSTagger
from nltk.corpus import wordnet as wn

spanish_postagger = POSTagger('models/spanish.tagger', 'stanford-postagger.jar', encoding='utf8')

from SPARQLWrapper import SPARQLWrapper, JSON
#sparql = SPARQLWrapper("http://dbpedia.org/sparql")
# ESTO ES PARA CONSULTAS EN ESPAÑOL
sparql = SPARQLWrapper("http://es.dbpedia.org/sparql")

#sentences = ['Acateyahualco. El copal se usa principalmente para sahumar en distintas ocasiones como lo son las fiestas religiosas, aunque también tiene usos medicinales y se extrae de una forma muy particular que las personas del ejido le llaman chimear que consiste en raspar la corteza del árbol de copal para usar la resina adherida a esta. Estas prácticas se han sido transmitidas de generación en generación.','11 nuestro país, las flores, hojas y frutos se usan para aliviar la tos y también se emplea como sedante; la corteza de la raíz, como diurético para enfermedades del riñón, para el control de la formación de venas varicosas, de peso y colesterol; y la raíz y frutos para la diabetes (Martínez, 1967). Adicionalmente, se usa como tónico cardiaco, para mejorar la circulación, el dolor de angina de pecho y dolores abdominales y para controlar la hipertensión (Ody, 1993).','El copal se usa principalmente como tónico cardiaco, para mejorar la circulación, el dolor de angina de pecho y dolores abdominales y para controlar la hipertensión']
sentences = ['El copal se usa principalmente para curar el cáncer de pulmón.','Las flores, hojas y frutos se usan para aliviar la tos y también se emplea como sedante.']

def isNoun(tag):
	return True if tag.startswith('n') else False

# def isSymptom(synset):
# 	if not synset:
# 		return False
# 	else:
# 		symptom = wn.synset('symptom.n.01')
# 		hyper = lambda s: s.hypernyms()
# 		return symptom in list(synset.closure(hyper))

# def isDisease(synset):
# 	if not synset:
# 		return False
# 	else:
# 		symptom = wn.synset('symptom.n.01')
# 		disease = wn.synset('disease.n.01')
# 		illness = [wn.synset('symptom.n.01'), wn.synset('disease.n.01'), wn.synset('symptom.n.01')]
# 		hyper = lambda s: s.hypernyms()
# 		return any((True for i in illness if i in list(synset.closure(hyper))))
# 		#return [symptom, disease] in list(synset.closure(hyper))

def isDisease(termino):
	sparql.setQuery(""" PREFIX dcterms: <http://purl.org/dc/terms/>
					ASK WHERE{
					?resource  rdf:type dbpedia-owl:Disease .
 					OPTIONAL{?resource foaf:name ?spanish_name }
 					FILTER(regex(?spanish_name,'"""+termino+"""'))}""")
	sparql.setReturnFormat(JSON)
	results = sparql.query().convert()
	#print results['boolean']
	return results['boolean']

for sent in sentences:
	
	words = sent.split()
	tagged_words = spanish_postagger.tag(words)

	nouns = []

	for (word, tag) in tagged_words:

		print(word+' '+tag).encode('utf8')
		if isNoun(tag): nouns.append(word)

	print(nouns)

	for n in nouns:
		print(n)
		if isDisease(n):
			print('\t************* BINGO! *************\t'+n+'')

# 		synsets = wn.synsets(n, pos=wn.NOUN, lang='spa')

# 		if synsets:
# 			print(synsets)
# 			for synset in synsets:
# 				#synset = synsets[0]
# 				if isDisease(synset):
# 					print('\t************* BINGO! *************\t'+n+'')



# sparql.setQuery("""
# PREFIX dcterms: <http://purl.org/dc/terms/>
# ASK WHERE{
#  ?resource  rdf:type dbpedia-owl:Disease .
#  OPTIONAL{?resource foaf:name ?spanish_name }
#  FILTER(regex(?spanish_name,'Acné'))
# }
# """)

# sparql.setReturnFormat(JSON)
# results = sparql.query().convert()
# print results['boolean']
# for result in results:
# 	print type(result)
# 	print str(result)
