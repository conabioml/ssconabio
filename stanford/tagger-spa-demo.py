#-*- coding: utf8 -*-

# about the tagger: http://nlp.stanford.edu/software/tagger.shtml 
# about the tagset: nlp.lsi.upc.edu/freeling/doc/tagsets/tagset-es.html

import os
import nltk
from nltk.tag.stanford import StanfordPOSTagger

# variables java
java_path = "/usr/bin/java"
os.environ['JAVAHOME'] = java_path
spamodel = '/Users/amolina/codigo_plantas/stanford-postagger-full-2014-10-26/models/spanish.tagger'
posjar = '/Users/amolina/codigo_plantas/stanford-postagger-full-2014-10-26/stanford-postagger.jar'


spanish_postagger = StanfordPOSTagger(model_filename =spamodel,
                                      path_to_jar = posjar,
                                      encoding='utf8')

sentences = [u'El copal se usa principalmente para sahumar en distintas ocasiones como lo son las fiestas religiosas, aunque también tiene usos medicinales y se extrae de una forma muy particular que las personas del ejido le llaman chimear que consiste en raspar la corteza del árbol de copal para usar la resina adherida a esta.',
             u'Estas prácticas se han sido transmitidas de generación en generación.',
             u'Nuestro país, las flores, hojas y frutos se usan para aliviar la tos y también se emplea como sedante; la corteza de la raíz, como diurético para enfermedades del riñón, para el control de la formación de venas varicosas, de peso y colesterol; y la raíz y frutos para la diabetes (Martínez, 1967).',
             u'Adicionalmente, se usa como tónico cardiaco, para mejorar la circulación, el dolor de angina de pecho y dolores abdominales y para controlar la hipertensión (Ody, 1993).',
             'El copal se usa principalmente como tónico cardiaco, para mejorar la circulación, el dolor de angina de pecho y dolores abdominales y para controlar la hipertensión']


# Lo que comienza con n es un sustantiuvo
def isNoun(tag):
    return True if tag.startswith('n') else False

for sent in sentences:
    # TODO: usar tokenizador en lugar de split
    words = sent.split()
    tagged_words = spanish_postagger.tag(words)

    nouns = []
    for (word, tag) in tagged_words:

        print(word+' '+tag).encode('utf8')
        if isNoun(tag):
            nouns.append(word)

    print(nouns)
