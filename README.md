# README #
Desarrollo de  clasificador para relaciones entre nombres cientificos,nombres comunes.

Asesor:

Dr. Alejandro Molina

Desarrolladores:

Edgar Francisco Moreno. 
	[edge.f.moreno@gmail.com] 
David Flores.
	[dflores.uam@gmail.com]

Para utilizar este sitema es necesario contar con:

Sistema operativo Linux o MAC [Windows no funciona correctamente].
python 2.7 
NLTK 3.0 o superior


Se integran archivos para entrenamiento pero bien se pueden agregar eliminar o modificar.

Lo mismo sucede con  archivos de prueba y para clasificación.

Instalación:

se clona el repositorio:

	git clone https://edgefmoreno@bitbucket.org/conabioml/ssconabio.git

ejecución:

	python relationClassifier.py <Opciones> <valor>

Opciones:

*   -h, --help            show this help message and exit
*   -p FILE, --posterioriTree=FILE
*                         Abre un arbol ya entrenado
*   -t DIR, --trainDir=DIR
*                         Directorio para entrenar un nuevo arbol
*   -v BOOLEAN, --verbose=BOOLEAN
*                         default = True  para cambiar escribe False
*   -e DIR, --evalDir=DIR
*                         Directorio con archivos para la evaluacion
*   -c DIR, --classDir=DIR
*                         Directorio con archivos para clasificar
*   -s NAMETREE, --nameTree=NAMETREE
*                         Nombre de arbol despues de entrenar
*   -q, --quiet           don't print status messages to stdout

*DIR		
		Estas opciones hacen referencia a los directorios donde se encontraran los archivos que van a ENTRENAR, EVALUAR O CLASIFICAR con el sitema.
		es necesario que se encuentre los dos archivos con el mismo nombre pero con su 
		respectiva extención, es decir, el archivo de texto en un .txt con sus respectivas anotaciones en un .ann

		Es de suma importancia que tengan el mismo nombre

Problemas por el NLTK no esta completamente instalado, para solucionarlo:

Instalar pip:

    apt-get update
    apt-get -y install python-pip

Instalar nltk:

    Instalar NLTK:   sudo pip install -U nltk
    Instalar Numpy (optional):  sudo pip install -U numpy
    Probar instalación: 
    python 
    >> import nltk

Si manda este error:
------
    Traceback (most recent call last):
    File "trainSetDir.py", line 18, in <module>
    import simplejson
    ImportError: No module named simplejson
------

Instalar simplejson:

    pip install simplejson

LINUX
    
    python
    import nltk
    nltk.download()
    d all

MAC-OS

    python
    import nltk
    nltk.download()
    aparecera una ventana dar click en descargar todo

En caso de encontrar:
    
    BOOM!
    check:
    <line>	
    X____x' 

Existen dos posibilidades, el archivo .ann no corresponde al .txt
o la lectura de formatos UTF-8 no es correcta, esto pasa en sistemas operativos Windows